from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('1/', views.ques1, name='ques1'),
    path('2/', views.ques2, name='ques2'),
    path('3/', views.ques3, name='ques3'),
    path('4/', views.ques4, name='ques4'),
    path('5/', views.ques5, name='ques5'),
]
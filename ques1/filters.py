from ques1.models import Matches, Deliveries
from django.db.models import Count, Sum, Avg

def get_number_of_matches_per_season():
    matches_per_season = Matches.objects.values('season').annotate(number_of_games=Count('season')).order_by('season')
    return matches_per_season


def get_wins_per_season():
    wins_per_season = Matches.objects.exclude(winner__isnull=True).exclude(winner__exact='').values('winner','season').annotate(wins=Count('winner')).order_by('season')
    years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
    winning_per_year = {}
    for i in list(wins_per_season):
        if i['winner'] not in winning_per_year:
            winning_per_year[i['winner']] = [0] * 10
            winning_per_year[i['winner']][years.index(int(i['season']))] = i['wins']
        else:
            winning_per_year[i['winner']][years.index(int(i['season']))] = i['wins']
    return winning_per_year, years


def get_extra_runs():
    match_id = Matches.objects.values('id').filter(season=2016)
    extra_runs = Deliveries.objects.values('bowling_team').filter(match_id__in=match_id).annotate(extra_runs = Sum('extra_runs'))
    return extra_runs


def get_most_mom():
    most_mom = Matches.objects.values('player_of_match').annotate(mom=Count('player_of_match')).order_by('-mom')[:10]
    return most_mom

def get_most_economical_bowler():
    match_id = Matches.objects.values('id').filter(season=2015)
    most_economical_bowler = Deliveries.objects.values('bowler').filter(match_id__in=match_id).annotate(economy= Avg('total_runs') * 6).order_by('economy')[:10]
    return most_economical_bowler
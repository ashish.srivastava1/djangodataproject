from django.shortcuts import render
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

def home(request):
    return render(request, 'home.html')


from ques1 import filters
@cache_page(CACHE_TTL)
def ques1(request):
    matches_per_season = filters.get_number_of_matches_per_season()
    return render(request, 'plot_graph.html', {'dataset': matches_per_season})

@cache_page(CACHE_TTL)
def ques2(request):
    wins_per_season, years = filters.get_wins_per_season()
    return render(request, 'plot_graph2.html', {'dataset': wins_per_season, 'years': years})

@cache_page(CACHE_TTL)
def ques3(request):
    extra_runs = filters.get_extra_runs()
    return render(request, 'plot_graph3.html', {'dataset': extra_runs})

@cache_page(CACHE_TTL)
def ques4(request):
    most_economical_bowler = filters.get_most_economical_bowler()
    return render(request, 'plot_graph4.html', {'dataset': most_economical_bowler})

@cache_page(CACHE_TTL)
def ques5(request):
    most_mom = filters.get_most_mom()
    return render(request, 'plot_graph5.html', {'dataset': most_mom})
from django.core.management.base import BaseCommand
import mysql.connector


class Command(BaseCommand):
    help = 'Loads the csv file into mysql database'

    def handle(self, *args, **kwargs):
        mydb = mysql.connector.connect(
            host="mydb.cviaagoh6lmy.us-east-2.rds.amazonaws.com",
            user="ashish",
            passwd="test1234",
            database="ddp",
            autocommit=True
        )
        mycursor = mydb.cursor()

        mycursor.execute("LOAD DATA LOCAL INFILE 'matches.csv' INTO TABLE ddp.ques1_matches FIELDS TERMINATED BY ','  IGNORE 1 LINES;")

        mycursor.execute("LOAD DATA LOCAL INFILE 'deliveries.csv' INTO TABLE ddp.ques1_deliveries FIELDS TERMINATED BY ','  IGNORE 1 LINES(match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder);")
        mycursor.close()
        mydb.close()
